Mister Sparky Electrical Is Ohio’s First Choice For Complete Electrical Service, Repair, Maintenance, And Installations. We Offer Same Day Service, 100% Satisfaction Guarantees, And Have A Team Of Industry Leading Master Electricians Ready To Tackle Any Electrical Need You May Have.

Address: 1602 W. Bancroft St, Toledo, OH 43606, USA

Phone: 419-473-3192

Website: https://www.mistersparkytoledo.com
